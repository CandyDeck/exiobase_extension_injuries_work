from selenium import webdriver
from time import sleep

def asbestos():
       
   
    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()

    #driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
   
    '''
    Asbestos
    '''
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-4"]').click()    
    sleep(30)
   
    url_table_asbestos = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()

    return url_table_asbestos

def arsenic():

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
   
    '''
    Arsenic
    '''
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-3"]').click()    
    sleep(30)
    url_table_arsenic = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_arsenic


def benzene():  
   
    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
   
    '''
    Benzene
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-5"]').click()    
    sleep(30)
    url_table_benzene = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_benzene


def beryllium():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)

    '''
    Beryllium
    '''

    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-6"]').click()    
    sleep(30)
    url_table_beryllium = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_beryllium


def cadmium():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
   
   
    '''
    Cadmium
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-7"]').click()    
    sleep(30)
    url_table_cadmium = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_cadmium


def chromium():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
    '''
    Chromium
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-8"]').click()    
    sleep(30)
    url_table_chromium = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_chromium

def diesel():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)

    '''
    Diesel
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-9"]').click()    
    sleep(30)
    url_table_diesel = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_diesel


def formaldehyde():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
   
   
    '''
    Formaldehyde
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-10"]').click()    
    sleep(30)
    url_table_formaldehyde = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()    
    return url_table_formaldehyde

def nickel():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)

    '''
    Nickel
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-11"]').click()    
    sleep(30)
    url_table_nickel = driver.find_element("id", 'cmd_export_data').get_attribute('href')    
   
    driver.close()
    driver.quit()    
    return url_table_nickel

def hydrocarbons():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
    '''
    hydrocarbons
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-12"]').click()    
    sleep(30)
    url_table_hydrocarbons = driver.find_element("id", 'cmd_export_data').get_attribute('href')  
    driver.close()
    driver.quit()
    return url_table_hydrocarbons

def silica():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
    '''
    Silica
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-13"]').click()    
    sleep(30)
    url_table_silica = driver.find_element("id", 'cmd_export_data').get_attribute('href')      
    driver.close()
    driver.quit()
    return url_table_silica


def sulfuric():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
    '''
    Sulfuric Acid
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-14"]').click()    
    sleep(30)
    url_table_sulfuric = driver.find_element("id", 'cmd_export_data').get_attribute('href')  
    driver.close()
    driver.quit()
    return url_table_sulfuric

def trichloroethylene():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
    '''
    Trichloroethylene
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-15"]').click()    
    sleep(30)
    url_table_trichloroethylene = driver.find_element("id", 'cmd_export_data').get_attribute('href')  
    driver.close()
    driver.quit()
    return url_table_trichloroethylene

def asthmagens():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
   
   
    '''
    Asthmagens
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-1"]').click()    
    sleep(30)
    url_table_asthmagens = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_asthmagens


def gases():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
   
    '''
    Gases
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-18"]').click()    
    sleep(30)
    url_table_gases = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_gases

def noise():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)


    '''
    Noise
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-17"]').click()    
    sleep(30)
    url_table_noise = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_noise


def injuries():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)

    '''
    Injuries
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-16"]').click()    
    sleep(30)
    url_table_injuries = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_injuries


def ergonomic():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)


    '''
    Ergonomic
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-2"]').click()    
    sleep(30)
    url_table_ergonomic = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_ergonomic    
   
def hours():  

    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
    driver = webdriver.Firefox()
    driver.maximize_window()
    driver.get(url)
    sleep(30)
   
    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
    sleep(30)
   
    '''
    Hours
    '''
   
    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div[3]/div/div/button').click()
    driver.find_element("xpath", '//*[@id="bs-select-19-0"]').click()    
    sleep(30)
    url_table_hours = driver.find_element("id", 'cmd_export_data').get_attribute('href')
    driver.close()
    driver.quit()
    return url_table_hours  
   


#    return url_table_asbestos
#,url_table_arsenic,url_table_asthmagens,url_table_benzene,url_table_beryllium,url_table_cadmium,url_table_chromium,url_table_diesel,url_table_ergonomic,url_table_formaldehyde,url_table_gases,url_table_hours,url_table_hydrocarbons,url_table_injuries,url_table_nickel,url_table_noise,url_table_silica,url_table_sulfuric,url_table_trichloroethylene


       
       
       

#    url = 'https://ec.europa.eu/eurostat/web/main/home'
#    driver = webdriver.Firefox()
#    driver.maximize_window()
#    driver.get(url)
#    
#    driver.find_element("xpath", '/html/body/div[2]/header/div[2]/section/div/div[2]/div/div[2]/div/ul/li[2]/a/span').click()
#    driver.find_element("xpath", '/html/body/div[2]/section/div/div/div[2]/div/div[2]/section/div/div[2]/div/div/form/div/div/div[2]/div[3]/a/table/tbody/tr/td[6]/span').click()
#    driver.find_element("xpath", '/html/body/div[2]/section/div/div/div[2]/div/div[2]/section/div/div[2]/div/div/form/div/div/div[2]/div[3]/div/div[4]/div/div[3]/div/a/table/tbody/tr/td[8]/span").click
#    
#    /html/body/div[2]/section/div/div/div[2]/div/div[2]/section/div/div[2]/div/div/form/div/div/div/div[1]/div/a/table/tbody/tr/td[5]/span
#    sleep(30)
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[17]/a/span[2]').click()    
#    sleep(30)
#    
#    url_table = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#    #driver.find_element("id", 'cmd_export_data').click()
#    
#    driver.close()
#    driver.quit()

   


#from selenium import webdriver
#from time import sleep
#
#def url_who():
#    url = 'https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/'
#    driver = webdriver.Firefox()
#    driver.maximize_window()
#    driver.get(url)
#    sleep(30)
#    
#    driver.find_element("xpath", '/html/body/nav/div/div[2]/ul/li[3]/a').click()
#    sleep(30)
#    
#    '''
#    Asbestos
#    '''
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[1]/a/span[2]').click()    
#    sleep(30)
#    url_table_asbestos = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#    driver.close()
#    driver.quit()
   
#    '''
#    Arsenic
#    '''
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[2]/a/span[2]').click()    
#    sleep(30)
#    url_table_arsenic = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#    
#    '''
#    Benzene
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[3]/a/span[2]').click()    
#    sleep(30)
#    url_table_benzene = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#
#    '''
#    Beryllium
#    '''
#
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()  
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[4]/a/span[2]').click()    
#    sleep(30)
#    url_table_beryllium = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#
#    '''
#    Cadmium
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[5]/a/span[2]').click()    
#    sleep(30)
#    url_table_cadmium = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#
#    '''
#    Chromium
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[6]/a/span[2]').click()    
#    sleep(30)
#    url_table_chromium = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#
#
#    '''
#    Diesel
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[7]/a/span[2]').click()    
#    sleep(30)
#    url_table_diesel = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#
#    '''
#    Formaldehyde
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[8]/a/span[2]').click()    
#    sleep(30)
#    url_table_formaldehyde = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#    
#    '''
#    Nickel
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[9]/a/span[2]').click()    
#    sleep(30)
#    url_table_nickel = driver.find_element("id", 'cmd_export_data').get_attribute('href')    
#    
#    '''
#    hydrocarbons
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[10]/a/span[2]').click()    
#    sleep(30)
#    url_table_hydrocarbons = driver.find_element("id", 'cmd_export_data').get_attribute('href')  
#    
#    '''
#    Silica
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[11]/a/span[2]').click()    
#    sleep(30)
#    url_table_silica = driver.find_element("id", 'cmd_export_data').get_attribute('href')      
#
#    '''
#    Sulfuric Acid
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[12]/a/span[2]').click()    
#    sleep(30)
#    url_table_sulfuric = driver.find_element("id", 'cmd_export_data').get_attribute('href')  
#
#    '''
#    Trichloroethylene
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[13]/a/span[2]').click()    
#    sleep(30)
#    url_table_trichloroethylene = driver.find_element("id", 'cmd_export_data').get_attribute('href')  
#
#
#    '''
#    Asthmagens
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[14]/a/span[2]').click()    
#    sleep(30)
#    url_table_asthmagens = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#
#    '''
#    Gases
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[15]/a/span[2]').click()    
#    sleep(30)
#    url_table_gases = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#
#    '''
#    Noise
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[16]/a/span[2]').click()    
#    sleep(30)
#    url_table_noise = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#
#    '''
#    Injuries
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[17]/a/span[2]').click()    
#    sleep(30)
#    url_table_injuries = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#
#    '''
#    Ergonomic
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[18]/a/span[2]').click()    
#    sleep(30)
#    url_table_ergonomic = driver.find_element("id", 'cmd_export_data').get_attribute('href')
#    
#    '''
#    Hours
#    '''
#    
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/button/div/div/div').click()
#    driver.find_element("xpath", '/html/body/div[1]/div/div[3]/div/div[1]/div/div/div/ul/li[19]/a/span[2]').click()    
#    sleep(30)
#    url_table_hours = driver.find_element("id", 'cmd_export_data').get_attribute('href')

   
#    return url_table_asbestos
#,url_table_arsenic,url_table_asthmagens,url_table_benzene,url_table_beryllium,url_table_cadmium,url_table_chromium,url_table_diesel,url_table_ergonomic,url_table_formaldehyde,url_table_gases,url_table_hours,url_table_hydrocarbons,url_table_injuries,url_table_nickel,url_table_noise,url_table_silica,url_table_sulfuric,url_table_trichloroethylene

