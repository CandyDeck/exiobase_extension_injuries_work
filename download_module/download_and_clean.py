def download_clean():
    from download import download_data
    from pathlib import Path
    import pandas as pd 
    import country_converter as coco
    import get_url
    
    from get_url import asbestos
    from get_url import arsenic
    from get_url import benzene
    from get_url import beryllium
    from get_url import asthmagens
    from get_url import cadmium
    from get_url import chromium
    from get_url import diesel
    from get_url import ergonomic
    from get_url import formaldehyde
    from get_url import gases
    from get_url import hours
    from get_url import hydrocarbons
    from get_url import injuries
    from get_url import nickel
    from get_url import noise
    from get_url import silica
    from get_url import sulfuric
    from get_url import trichloroethylene
    
    storage_group_root = Path(".").absolute()
    download_path = storage_group_root / "download"
    data_path = storage_group_root / "data"
    data_cleaned_path = storage_group_root / "clean_data"
    data_distribution_path = storage_group_root / "distribution_data"
    data_cleaned_path.mkdir(parents=True, exist_ok=True)
    data_distribution_path.mkdir(parents=True, exist_ok=True)



    src_url = get_url.asbestos()
    src_csv = Path("asbestos.csv")
    download_data(src_url,src_csv,data_path)

    src_url = arsenic()
    src_csv = Path("arsenic.csv")
    download_data(src_url,src_csv,data_path)

    src_url = benzene()
    src_csv = Path("benzene.csv")
    download_data(src_url,src_csv,data_path)

    src_url = beryllium()
    src_csv = Path("beryllium.csv")
    download_data(src_url,src_csv,data_path)


    src_url = asthmagens()
    src_csv = Path("asthmagens.csv")
    download_data(src_url, src_csv, data_path)

    src_url = cadmium()
    src_csv = Path("cadmium.csv")
    download_data(src_url,src_csv,data_path)

    src_url = chromium()
    src_csv = Path("chromium.csv")
    download_data(src_url,src_csv,data_path)

    src_url = diesel()
    src_csv = Path("diesel.csv")
    download_data(src_url,src_csv,data_path)

    src_url = ergonomic()
    src_csv = Path("ergonomic.csv")
    download_data(src_url,src_csv,data_path)

    src_url = formaldehyde()
    src_csv = Path("formaldehyde.csv")
    download_data(src_url,src_csv,data_path)

    src_url = gases()
    src_csv = Path("gases.csv")
    download_data(src_url,src_csv,data_path)

    src_url = hours()
    src_csv = Path("hours.csv")
    download_data(src_url,src_csv,data_path)

    src_url = hydrocarbons()
    src_csv = Path("hydrocarbons.csv")
    download_data(src_url,src_csv,data_path)

    src_url = injuries()
    src_csv = Path("injuries.csv")
    download_data(src_url,src_csv,data_path)

    src_url = nickel()
    src_csv = Path("nickel.csv")
    download_data(src_url,src_csv,data_path)

    src_url = noise()
    src_csv = Path("noise.csv")
    download_data(src_url,src_csv,data_path)

    src_url = silica()
    src_csv = Path("silica.csv")
    download_data(src_url,src_csv,data_path)

    src_url = sulfuric()
    src_csv = Path("sulfuric.csv")
    download_data(src_url,src_csv,data_path)

    src_url = trichloroethylene()
    src_csv = Path("trichloroethylene.csv")
    download_data(src_url,src_csv,data_path)

    cc_all = coco.CountryConverter(include_obsolete=True)

    asbestos = pd.read_csv(data_path/"asbestos.csv", encoding="utf-8-sig") 

    asbestos = asbestos[asbestos['Age group'].str.contains('≥15 years',regex=True)]
    asbestos = asbestos[asbestos['Sex'].str.contains('Male|Female',regex=True)]
    asbestos = asbestos[asbestos['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    asbestos=asbestos.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    asbestos = asbestos[asbestos['Year'] >1994]

    country_code = list(asbestos['Country'])

    asbestos.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    asbestos.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    asbestos = asbestos[~asbestos['EXIO3'].str.contains('not found',regex=True)]
    asbestos=asbestos.drop(columns=['Risk Factor','Age group'])

    asbestos.to_csv(data_cleaned_path/"asbestos_cleaned.csv",index=False)

    arsenic = pd.read_csv(data_path/"arsenic.csv", encoding="utf-8-sig") 

    arsenic = arsenic[arsenic['Age group'].str.contains('≥15 years',regex=True)]
    arsenic = arsenic[arsenic['Sex'].str.contains('Male|Female',regex=True)]
    arsenic = arsenic[arsenic['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    arsenic=arsenic.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    arsenic = arsenic[arsenic['Year'] >1994]

    country_code = list(arsenic['Country'])

    arsenic.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    arsenic.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    arsenic = arsenic[~arsenic['EXIO3'].str.contains('not found',regex=True)]
    arsenic=arsenic.drop(columns=['Risk Factor','Age group'])

    arsenic.to_csv(data_cleaned_path/"arsenic_cleaned.csv",index=False)


    benzene = pd.read_csv(data_path/"benzene.csv", encoding="utf-8-sig") 

    benzene = benzene[benzene['Age group'].str.contains('≥15 years',regex=True)]
    benzene = benzene[benzene['Sex'].str.contains('Male|Female',regex=True)]
    benzene = benzene[benzene['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    benzene=benzene.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    benzene = benzene[benzene['Year'] >1994]

    country_code = list(benzene['Country'])

    benzene.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    benzene.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    benzene = benzene[~benzene['EXIO3'].str.contains('not found',regex=True)]
    benzene=benzene.drop(columns=['Risk Factor','Age group'])

    benzene.to_csv(data_cleaned_path/"benzene_cleaned.csv",index=False)


    beryllium = pd.read_csv(data_path/"beryllium.csv", encoding="utf-8-sig") 

    beryllium = beryllium[beryllium['Age group'].str.contains('≥15 years',regex=True)]
    beryllium = beryllium[beryllium['Sex'].str.contains('Male|Female',regex=True)]
    beryllium = beryllium[beryllium['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    beryllium=beryllium.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    beryllium = beryllium[beryllium['Year'] >1994]

    country_code = list(beryllium['Country'])
    
    beryllium.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    beryllium.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    beryllium = beryllium[~beryllium['EXIO3'].str.contains('not found',regex=True)]
    beryllium=beryllium.drop(columns=['Risk Factor','Age group'])

    beryllium.to_csv(data_cleaned_path/"beryllium_cleaned.csv",index=False)
    
    asthmagens = pd.read_csv(data_path/"asthmagens.csv", encoding="utf-8-sig") 

    asthmagens = asthmagens[asthmagens['Age group'].str.contains('≥15 years',regex=True)]
    asthmagens = asthmagens[asthmagens['Sex'].str.contains('Male|Female',regex=True)]
    asthmagens = asthmagens[asthmagens['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    asthmagens=asthmagens.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    asthmagens = asthmagens[asthmagens['Year'] >1994]

    country_code = list(asthmagens['Country'])

    asthmagens.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    asthmagens.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    asthmagens = asthmagens[~asthmagens['EXIO3'].str.contains('not found',regex=True)]
    asthmagens=asthmagens.drop(columns=['Risk Factor','Age group'])

    asthmagens.to_csv(data_cleaned_path/"asthmagens_cleaned.csv",index=False)

    cadmium = pd.read_csv(data_path/"cadmium.csv", encoding="utf-8-sig") 

    cadmium = cadmium[cadmium['Age group'].str.contains('≥15 years',regex=True)]
    cadmium = cadmium[cadmium['Sex'].str.contains('Male|Female',regex=True)]
    cadmium = cadmium[cadmium['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    cadmium=cadmium.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    cadmium = cadmium[cadmium['Year'] >1994]

    country_code = list(cadmium['Country'])

    cadmium.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    cadmium.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    cadmium = cadmium[~cadmium['EXIO3'].str.contains('not found',regex=True)]
    cadmium=cadmium.drop(columns=['Risk Factor','Age group'])

    cadmium.to_csv(data_cleaned_path/"cadmium_cleaned.csv",index=False)


    chromium = pd.read_csv(data_path/"chromium.csv", encoding="utf-8-sig") 

    chromium = chromium[chromium['Age group'].str.contains('≥15 years',regex=True)]
    chromium = chromium[chromium['Sex'].str.contains('Male|Female',regex=True)]
    chromium = chromium[chromium['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    chromium=chromium.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    chromium = chromium[chromium['Year'] >1994]

    country_code = list(chromium['Country'])

    chromium.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    chromium.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    chromium = chromium[~chromium['EXIO3'].str.contains('not found',regex=True)]
    chromium=chromium.drop(columns=['Risk Factor','Age group'])

    chromium.to_csv(data_cleaned_path/"chromium_cleaned.csv",index=False)

    diesel = pd.read_csv(data_path/"diesel.csv", encoding="utf-8-sig") 

    diesel = diesel[diesel['Age group'].str.contains('≥15 years',regex=True)]
    diesel = diesel[diesel['Sex'].str.contains('Male|Female',regex=True)]
    diesel = diesel[diesel['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    diesel=diesel.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    diesel = diesel[diesel['Year'] >1994]

    country_code = list(diesel['Country'])

    diesel.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    diesel.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    diesel = diesel[~diesel['EXIO3'].str.contains('not found',regex=True)]
    diesel=diesel.drop(columns=['Risk Factor','Age group'])

    diesel.to_csv(data_cleaned_path/"diesel_cleaned.csv",index=False)

    ergonomic = pd.read_csv(data_path/"ergonomic.csv", encoding="utf-8-sig") 

    ergonomic = ergonomic[ergonomic['Age group'].str.contains('≥15 years',regex=True)]
    ergonomic = ergonomic[ergonomic['Sex'].str.contains('Male|Female',regex=True)]
    ergonomic = ergonomic[ergonomic['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    ergonomic=ergonomic.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    ergonomic = ergonomic[ergonomic['Year'] >1994]

    country_code = list(ergonomic['Country'])

    ergonomic.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    ergonomic.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    ergonomic = ergonomic[~ergonomic['EXIO3'].str.contains('not found',regex=True)]
    ergonomic=ergonomic.drop(columns=['Risk Factor','Age group'])

    ergonomic.to_csv(data_cleaned_path/"ergonomic_cleaned.csv",index=False)

    formaldehyde = pd.read_csv(data_path/"formaldehyde.csv", encoding="utf-8-sig") 

    formaldehyde = formaldehyde[formaldehyde['Age group'].str.contains('≥15 years',regex=True)]
    formaldehyde = formaldehyde[formaldehyde['Sex'].str.contains('Male|Female',regex=True)]
    formaldehyde = formaldehyde[formaldehyde['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    formaldehyde=formaldehyde.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    formaldehyde = formaldehyde[formaldehyde['Year'] >1994]

    country_code = list(formaldehyde['Country'])

    formaldehyde.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    formaldehyde.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    formaldehyde = formaldehyde[~formaldehyde['EXIO3'].str.contains('not found',regex=True)]
    formaldehyde=formaldehyde.drop(columns=['Risk Factor','Age group'])

    formaldehyde.to_csv(data_cleaned_path/"formaldehyde_cleaned.csv",index=False)

    gases = pd.read_csv(data_path/"gases.csv", encoding="utf-8-sig") 

    gases = gases[gases['Age group'].str.contains('≥15 years',regex=True)]
    gases = gases[gases['Sex'].str.contains('Male|Female',regex=True)]
    gases = gases[gases['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    gases=gases.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    gases = gases[gases['Year'] >1994]

    country_code = list(gases['Country'])

    gases.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    gases.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    gases = gases[~gases['EXIO3'].str.contains('not found',regex=True)]
    gases=gases.drop(columns=['Risk Factor','Age group'])

    gases.to_csv(data_cleaned_path/"gases_cleaned.csv",index=False)

    hours = pd.read_csv(data_path/"hours.csv", encoding="utf-8-sig") 

    hours = hours[hours['Age group'].str.contains('≥15 years',regex=True)]
    hours = hours[hours['Sex'].str.contains('Male|Female',regex=True)]
    hours = hours[hours['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    hours=hours.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    hours = hours[hours['Year'] >1994]

    country_code = list(hours['Country'])

    hours.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    hours.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    hours = hours[~hours['EXIO3'].str.contains('not found',regex=True)]
    hours=hours.drop(columns=['Risk Factor','Age group'])

    hours.to_csv(data_cleaned_path/"hours_cleaned.csv",index=False)


    hydrocarbons = pd.read_csv(data_path/"hydrocarbons.csv", encoding="utf-8-sig") 

    hydrocarbons = hydrocarbons[hydrocarbons['Age group'].str.contains('≥15 years',regex=True)]
    hydrocarbons = hydrocarbons[hydrocarbons['Sex'].str.contains('Male|Female',regex=True)]
    hydrocarbons = hydrocarbons[hydrocarbons['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    hydrocarbons=hydrocarbons.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    hydrocarbons = hydrocarbons[hydrocarbons['Year'] >1994]

    country_code = list(hydrocarbons['Country'])

    hydrocarbons.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    hydrocarbons.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    hydrocarbons = hydrocarbons[~hydrocarbons['EXIO3'].str.contains('not found',regex=True)]
    hydrocarbons=hydrocarbons.drop(columns=['Risk Factor','Age group'])

    hydrocarbons.to_csv(data_cleaned_path/"hydrocarbons_cleaned.csv",index=False)

    injuries = pd.read_csv(data_path/"injuries.csv", encoding="utf-8-sig") 

    injuries = injuries[injuries['Age group'].str.contains('≥15 years',regex=True)]
    injuries = injuries[injuries['Sex'].str.contains('Male|Female',regex=True)]
    injuries = injuries[injuries['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    injuries=injuries.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    injuries = injuries[injuries['Year'] >1994]

    country_code = list(injuries['Country'])

    injuries.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    injuries.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    injuries = injuries[~injuries['EXIO3'].str.contains('not found',regex=True)]
    injuries=injuries.drop(columns=['Risk Factor','Age group'])

    injuries.to_csv(data_cleaned_path/"injuries_cleaned.csv",index=False)

    nickel = pd.read_csv(data_path/"nickel.csv", encoding="utf-8-sig") 

    nickel = nickel[nickel['Age group'].str.contains('≥15 years',regex=True)]
    nickel = nickel[nickel['Sex'].str.contains('Male|Female',regex=True)]
    nickel = nickel[nickel['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    nickel=nickel.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    nickel = nickel[nickel['Year'] >1994]

    country_code = list(nickel['Country'])

    nickel.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    nickel.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    nickel = nickel[~nickel['EXIO3'].str.contains('not found',regex=True)]
    nickel=nickel.drop(columns=['Risk Factor','Age group'])

    nickel.to_csv(data_cleaned_path/"nickel_cleaned.csv",index=False)

    noise = pd.read_csv(data_path/"noise.csv", encoding="utf-8-sig") 

    noise = noise[noise['Age group'].str.contains('≥15 years',regex=True)]
    noise = noise[noise['Sex'].str.contains('Male|Female',regex=True)]
    noise = noise[noise['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    noise=noise.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    noise = noise[noise['Year'] >1994]

    country_code = list(noise['Country'])

    noise.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    noise.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    noise = noise[~noise['EXIO3'].str.contains('not found',regex=True)]
    noise=noise.drop(columns=['Risk Factor','Age group'])

    noise.to_csv(data_cleaned_path/"noise_cleaned.csv",index=False)

    silica = pd.read_csv(data_path/"silica.csv", encoding="utf-8-sig") 

    silica = silica[silica['Age group'].str.contains('≥15 years',regex=True)]
    silica = silica[silica['Sex'].str.contains('Male|Female',regex=True)]
    silica = silica[silica['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    silica=silica.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    silica = silica[silica['Year'] >1994]

    country_code = list(silica['Country'])

    silica.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    silica.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    silica = silica[~silica['EXIO3'].str.contains('not found',regex=True)]
    silica=silica.drop(columns=['Risk Factor','Age group'])

    silica.to_csv(data_cleaned_path/"silica_cleaned.csv",index=False)

    sulfuric = pd.read_csv(data_path/"sulfuric.csv", encoding="utf-8-sig") 

    sulfuric = sulfuric[sulfuric['Age group'].str.contains('≥15 years',regex=True)]
    sulfuric = sulfuric[sulfuric['Sex'].str.contains('Male|Female',regex=True)]
    sulfuric = sulfuric[sulfuric['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    sulfuric=sulfuric.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    sulfuric = sulfuric[sulfuric['Year'] >1994]

    country_code = list(sulfuric['Country'])

    sulfuric.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    sulfuric.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    sulfuric = sulfuric[~sulfuric['EXIO3'].str.contains('not found',regex=True)]
    sulfuric=sulfuric.drop(columns=['Risk Factor','Age group'])

    sulfuric.to_csv(data_cleaned_path/"sulfuric_cleaned.csv",index=False)

    trichloroethylene = pd.read_csv(data_path/"trichloroethylene.csv", encoding="utf-8-sig") 

    trichloroethylene = trichloroethylene[trichloroethylene['Age group'].str.contains('≥15 years',regex=True)]
    trichloroethylene = trichloroethylene[trichloroethylene['Sex'].str.contains('Male|Female',regex=True)]
    trichloroethylene = trichloroethylene[trichloroethylene['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    trichloroethylene=trichloroethylene.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    trichloroethylene = trichloroethylene[trichloroethylene['Year'] >1994]

    country_code = list(trichloroethylene['Country'])

    trichloroethylene.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    trichloroethylene.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    trichloroethylene = trichloroethylene[~trichloroethylene['EXIO3'].str.contains('not found',regex=True)]
    trichloroethylene=trichloroethylene.drop(columns=['Risk Factor','Age group'])

    trichloroethylene.to_csv(data_cleaned_path/"trichloroethylene_cleaned.csv",index=False)	


'''
TO be added and downloaded ! 



src_url2 = "https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/hsw_ac5b_tabular.tsv.gz"
src_csv2 = Path("hsw_ac5b_tabular.tsv")
'''
