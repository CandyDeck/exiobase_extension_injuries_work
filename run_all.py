""" Occupationnal Injuries processing


"""

#from pathlib import Path
import sys
#import os


sys.path.insert(1, 'download_module')
sys.path.insert(2,'all_scripts_in_serie')


import download_module.download_and_clean
import all_scripts_in_serie.exposure_all
import final_daly_exposure


# Step 1 - downloading and cleanig the data
download_module.download_and_clean.download_clean()
# Step 2 - processing the raw data related to each exposure
all_scripts_in_serie.exposure_all.exposure()
# Step 3 - producing the final table, one per occupational injurie
final_daly_exposure.final()
