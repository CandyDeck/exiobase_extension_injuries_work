

def final():
    
    import pandas as pd
    import country_converter as coco
    import ray
    from pathlib import Path
    import os
    
    cc_all = coco.CountryConverter(include_obsolete=True)
    
    mapping_injuries = pd.read_excel('aux/Mapping_Injuries_ILO-WHO_EXIOBASE_2023.xlsx','Sheet1')
    
    list_exposure=[]
    
    
    storage_group_root = Path(".").absolute()
    data_path = storage_group_root / "data"
    data_cleaned_path = storage_group_root / "clean_data"
    data_distribution_path = storage_group_root / "distribution_data"
    final_path = storage_group_root / "final_table"
    final_path.mkdir(parents=True, exist_ok=True)
    
    
    list_exposure=[]
    for a in os.listdir(data_cleaned_path):
        list_exposure.append(a.replace("_cleaned.csv",""))
    
    if "injuries" in list_exposure:
        list_exposure.remove('injuries')
    if "hours" in list_exposure:
        list_exposure.remove('hours')
    

    for expo in list_exposure:
        print(expo)
    
        daly = pd.read_csv('clean_data/'+expo+'_cleaned.csv')
        distrib_sector_femme = pd.read_csv('distribution_data/'+expo+'_distribution_sector_femme.csv')
        distrib_sector_homme = pd.read_csv('distribution_data/'+expo+'_distribution_sector_homme.csv')
        distrib_aggreg_femme = pd.read_csv('distribution_data/'+expo+'_distribution_femme_sector_aggregation.csv')
        distrib_aggreg_homme = pd.read_csv('distribution_data/'+expo+'_distribution_homme_sector_aggregation.csv')
    
        '''
        We do not take into consideration the Cause.
        This can be included later if needed by keeping the column Cause in df daly and by including it in daly_split.
        This needs also to include a new for loop below with : 
            for d in daly['Cause'].uniques :
        and by changing  d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':[expo]}
        '''
        daly =daly.drop(columns=['Cause'])
        
        
        daly=daly.groupby(['Year','Region','Country','ISO3','EXIO3','Sex','Measure','Estimate']).sum()
        daly=daly.reset_index()
        column_daly =  ['year','ISO3','EXIO3','sector','Mapping','Sex','estimate','DALY (in 1000s)','exposure']
        
        daly_split = pd.DataFrame(columns=column_daly)
        final = pd.DataFrame(columns=column_daly)
        
        ray.init(num_cpus = os.cpu_count()-1)
        
        @ray.remote
        
        def calcul_dalys(code,daly_split):
        
            for year in range(2009,2018):
                print(code,year)
                for c in distrib_sector_femme['Sector'].unique():
                    for b in daly['Estimate'].unique():
                        for a in daly['Sex'].unique():
                            if a == 'Female' :
                                daly_2010 = float(daly.loc[(daly['Year']==2010)&(daly['ISO3']==code)&(daly['Sex']=='Female')&(daly['Estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
                                daly_2016 = float(daly.loc[(daly['Year']==2016)&(daly['ISO3']==code)&(daly['Sex']=='Female')&(daly['Estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
                                delta_daly = (daly_2016-daly_2010)/(2016-2010)
                                if (year == 2010 or year==2016):
                                    value_aggreg = float(distrib_aggreg_femme.loc[(distrib_aggreg_femme['ISO3']==code)&(distrib_aggreg_femme['time']==year),['Value']].to_string(header=False, index=False))
                                    value_non_aggreg = float(distrib_sector_femme.loc[(distrib_sector_femme['ISO3']==code)&(distrib_sector_femme['time']==year)&(distrib_sector_femme['Sector']==c),['Value']].to_string(header=False, index=False))
                                    dalys = float(daly.loc[(daly['Year']==year)&(daly['ISO3']==code)&(daly['Sex']=='Female')&(daly['Estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
                                    if value_aggreg !=0 :
                                        dalys_sector= value_non_aggreg * dalys / value_aggreg
                                        
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                    else :
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                elif (year==2009):
                                    value_aggreg = float(distrib_aggreg_femme.loc[(distrib_aggreg_femme['ISO3']==code)&(distrib_aggreg_femme['time']==year),['Value']].to_string(header=False, index=False))
                                    value_non_aggreg = float(distrib_sector_femme.loc[(distrib_sector_femme['ISO3']==code)&(distrib_sector_femme['time']==year)&(distrib_sector_femme['Sector']==c),['Value']].to_string(header=False, index=False))
                                    dalys_2009 = daly_2010-delta_daly
                                    if (dalys_2009 > 0 and value_aggreg !=0):
                                        dalys_sector= value_non_aggreg * dalys_2009 / value_aggreg
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                    else:
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                        
                                        
                                elif (year>2010 and year <2016):
                                    value_aggreg = float(distrib_aggreg_femme.loc[(distrib_aggreg_femme['ISO3']==code)&(distrib_aggreg_femme['time']==year),['Value']].to_string(header=False, index=False))
                                    value_non_aggreg = float(distrib_sector_femme.loc[(distrib_sector_femme['ISO3']==code)&(distrib_sector_femme['time']==year)&(distrib_sector_femme['Sector']==c),['Value']].to_string(header=False, index=False))
                                    dalys = daly_2010+(year-2010)*delta_daly
                                    if value_aggreg !=0 :
                                        dalys_sector= value_non_aggreg * dalys / value_aggreg
        
        
                                    if (dalys_sector > 0 and value_aggreg !=0):
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                    else:
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                    
                                else :
                                    value_aggreg = float(distrib_aggreg_femme.loc[(distrib_aggreg_femme['ISO3']==code)&(distrib_aggreg_femme['time']==year),['Value']].to_string(header=False, index=False))
                                    value_non_aggreg = float(distrib_sector_femme.loc[(distrib_sector_femme['ISO3']==code)&(distrib_sector_femme['time']==year)&(distrib_sector_femme['Sector']==c),['Value']].to_string(header=False, index=False))
                                    dalys_2010_2016 = daly_2016+delta_daly
                                    if value_aggreg !=0 :
                                        dalys_sector= value_non_aggreg * dalys_2010_2016 / value_aggreg
        
                                    if (dalys_sector > 0 and value_aggreg !=0):
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])  
                                    else:
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                    
                            if a == 'Male' : 
                                daly_2010 = float(daly.loc[(daly['Year']==2010)&(daly['ISO3']==code)&(daly['Sex']=='Male')&(daly['Estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
                                daly_2016 = float(daly.loc[(daly['Year']==2016)&(daly['ISO3']==code)&(daly['Sex']=='Male')&(daly['Estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
                                delta_daly = (daly_2016-daly_2010)/(2016-2010)
                                if (year == 2010 or year==2016):
                                    value_aggreg = float(distrib_aggreg_homme.loc[(distrib_aggreg_homme['ISO3']==code)&(distrib_aggreg_homme['time']==year),['Value']].to_string(header=False, index=False))
                                    value_non_aggreg = float(distrib_sector_homme.loc[(distrib_sector_homme['ISO3']==code)&(distrib_sector_homme['time']==year)&(distrib_sector_homme['Sector']==c),['Value']].to_string(header=False, index=False))
                                    dalys = float(daly.loc[(daly['Year']==year)&(daly['ISO3']==code)&(daly['Sex']=='Male')&(daly['Estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
                                    if value_aggreg !=0:
                                        dalys_sector= value_non_aggreg * dalys / value_aggreg
        
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                    else:
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                elif (year==2009):
                                    value_aggreg = float(distrib_aggreg_homme.loc[(distrib_aggreg_homme['ISO3']==code)&(distrib_aggreg_homme['time']==year),['Value']].to_string(header=False, index=False))
                                    value_non_aggreg = float(distrib_sector_homme.loc[(distrib_sector_homme['ISO3']==code)&(distrib_sector_homme['time']==year)&(distrib_sector_homme['Sector']==c),['Value']].to_string(header=False, index=False))
                                    dalys_2009 = daly_2010-delta_daly
                                    if (dalys_2009 > 0 and value_aggreg !=0):
                                        dalys_sector= value_non_aggreg * dalys_2009 / value_aggreg
        
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                    else:
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                
                                elif (year>2010 and year <2016):
                                    value_aggreg = float(distrib_aggreg_homme.loc[(distrib_aggreg_homme['ISO3']==code)&(distrib_aggreg_homme['time']==year),['Value']].to_string(header=False, index=False))
                                    value_non_aggreg = float(distrib_sector_homme.loc[(distrib_sector_homme['ISO3']==code)&(distrib_sector_homme['time']==year)&(distrib_sector_homme['Sector']==c),['Value']].to_string(header=False, index=False))
                                    dalys = daly_2010+(year-2010)*delta_daly
                                    if (dalys>0 and value_aggreg !=0):
                                        
                                        dalys_sector= value_non_aggreg * dalys / value_aggreg
        
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                    else:
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
                                else :
                                    value_aggreg = float(distrib_aggreg_homme.loc[(distrib_aggreg_homme['ISO3']==code)&(distrib_aggreg_homme['time']==year),['Value']].to_string(header=False, index=False))
                                    value_non_aggreg = float(distrib_sector_homme.loc[(distrib_sector_homme['ISO3']==code)&(distrib_sector_homme['time']==year)&(distrib_sector_homme['Sector']==c),['Value']].to_string(header=False, index=False))
                                    dalys_2010_2016 = daly_2016+delta_daly
                                    if dalys_2010_2016 > 0:
                                        if (value_aggreg != 0):
                                            dalys_sector= value_non_aggreg * dalys_2010_2016 / value_aggreg
                                        
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])                            
                                    else:
                                        d={'year':[year],'ISO3': [code],'EXIO3':[cc_all.convert(names = code,src="ISO3", to='EXIO3')],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':[expo]}
                                        df = pd.DataFrame(data=d)
                                        daly_split=pd.concat([daly_split, df])
        
            #print(code, year, daly_split)
            #final=pd.concat([final, daly_split])
            return daly_split
        
        
        
        results = [ray.get([calcul_dalys.remote(code,daly_split) for code in daly['ISO3'].unique()])]
    
        
        
                
        for a in results : 
            final_table= pd.concat(a)
        #results = [ray.get(do_some_work.remote(x)) for x in range(4)]
        #df = pd.DataFrame(results, columns =['year','ISO3', 'EXIO3','estimate DALY (in 1000s)','exposure']) 
        final_table.to_csv('final_table/' +expo+ '_daly_iso3_ray.csv',index=False)
    
        
        
        ray.shutdown()
    
    
    
    # for year in range(2016,2017):
    #    print(year)
    
    
    # for year in range(2009,2018):
    #     print(year)
    #     for code in daly_exio3['EXIO3'].unique():
    #         print(code)
    #         for c in distrib_sector_femme_exio3['Sector'].unique():
    #             for b in daly_exio3['estimate'].unique():
    #                 for a in daly_exio3['sex'].unique():
    #                     if a == 'Female' :
    #                         daly_2010 = float(daly_exio3.loc[(daly_exio3['year']==2010)&(daly_exio3['EXIO3']==code)&(daly_exio3['sex']=='Female')&(daly_exio3['estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
    #                         daly_2016 = float(daly_exio3.loc[(daly_exio3['year']==2016)&(daly_exio3['EXIO3']==code)&(daly_exio3['sex']=='Female')&(daly_exio3['estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
    #                         delta_daly = (daly_2016-daly_2010)/(2016-2010)
    #                         if (year == 2010 or year==2016):
    #                             value_aggreg = float(distrib_aggreg_femme_exio3.loc[(distrib_aggreg_femme_exio3['EXIO3']==code)&(distrib_aggreg_femme_exio3['time']==year),['Value']].to_string(header=False, index=False))
    #                             value_non_aggreg = float(distrib_sector_femme_exio3.loc[(distrib_sector_femme_exio3['EXIO3']==code)&(distrib_sector_femme_exio3['time']==year)&(distrib_sector_femme_exio3['Sector']==c),['Value']].to_string(header=False, index=False))
    #                             dalys = float(daly_exio3.loc[(daly_exio3['year']==year)&(daly_exio3['EXIO3']==code)&(daly_exio3['sex']=='Female')&(daly_exio3['estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
    #                             if value_aggreg !=0 :
    #                                 dalys_sector= value_non_aggreg * dalys / value_aggreg
                                    
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    #                             else :
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    #                         elif (year==2009):
    #                             value_aggreg = float(distrib_aggreg_femme_exio3.loc[(distrib_aggreg_femme_exio3['EXIO3']==code)&(distrib_aggreg_femme_exio3['time']==year),['Value']].to_string(header=False, index=False))
    #                             value_non_aggreg = float(distrib_sector_femme_exio3.loc[(distrib_sector_femme_exio3['EXIO3']==code)&(distrib_sector_femme_exio3['time']==year)&(distrib_sector_femme_exio3['Sector']==c),['Value']].to_string(header=False, index=False))
    #                             dalys_2009 = daly_2010-delta_daly
    #                             if (dalys_2009 > 0 and value_aggreg !=0):
    #                                 dalys_sector= value_non_aggreg * dalys_2009 / value_aggreg
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    #                             else:
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
                                    
                                    
    #                         elif (year>2010 and year <2016):
    #                             value_aggreg = float(distrib_aggreg_femme_exio3.loc[(distrib_aggreg_femme_exio3['EXIO3']==code)&(distrib_aggreg_femme_exio3['time']==year),['Value']].to_string(header=False, index=False))
    #                             value_non_aggreg = float(distrib_sector_femme_exio3.loc[(distrib_sector_femme_exio3['EXIO3']==code)&(distrib_sector_femme_exio3['time']==year)&(distrib_sector_femme_exio3['Sector']==c),['Value']].to_string(header=False, index=False))
    #                             dalys = daly_2010+(year-2010)*delta_daly
    #                             if value_aggreg !=0 :
    #                                 dalys_sector= value_non_aggreg * dalys / value_aggreg
    
    
    #                             if (dalys_sector > 0 and value_aggreg !=0):
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    #                             else:
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
                                
    #                         else :
    #                             value_aggreg = float(distrib_aggreg_femme_exio3.loc[(distrib_aggreg_femme_exio3['EXIO3']==code)&(distrib_aggreg_femme_exio3['time']==year),['Value']].to_string(header=False, index=False))
    #                             value_non_aggreg = float(distrib_sector_femme_exio3.loc[(distrib_sector_femme_exio3['EXIO3']==code)&(distrib_sector_femme_exio3['time']==year)&(distrib_sector_femme_exio3['Sector']==c),['Value']].to_string(header=False, index=False))
    #                             dalys_2010_2016 = daly_2016+delta_daly
    #                             if value_aggreg !=0 :
    #                                 dalys_sector= value_non_aggreg * dalys_2010_2016 / value_aggreg
    
    #                             if (dalys_sector > 0 and value_aggreg !=0):
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])  
    #                             else:
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
                                
    #                     if a == 'Male' : 
    #                         daly_2010 = float(daly_exio3.loc[(daly_exio3['year']==2010)&(daly_exio3['EXIO3']==code)&(daly_exio3['sex']=='Male')&(daly_exio3['estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
    #                         daly_2016 = float(daly_exio3.loc[(daly_exio3['year']==2016)&(daly_exio3['EXIO3']==code)&(daly_exio3['sex']=='Male')&(daly_exio3['estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
    #                         delta_daly = (daly_2016-daly_2010)/(2016-2010)
    #                         if (year == 2010 or year==2016):
    #                             value_aggreg = float(distrib_aggreg_homme_exio3.loc[(distrib_aggreg_homme_exio3['EXIO3']==code)&(distrib_aggreg_homme_exio3['time']==year),['Value']].to_string(header=False, index=False))
    #                             value_non_aggreg = float(distrib_sector_homme_exio3.loc[(distrib_sector_homme_exio3['EXIO3']==code)&(distrib_sector_homme_exio3['time']==year)&(distrib_sector_homme_exio3['Sector']==c),['Value']].to_string(header=False, index=False))
    #                             dalys = float(daly_exio3.loc[(daly_exio3['year']==year)&(daly_exio3['EXIO3']==code)&(daly_exio3['sex']=='Male')&(daly_exio3['estimate']==b),['Number (for DALYs in 1000s)']].to_string(header=False,index=False))
    #                             if value_aggreg !=0:
    #                                 dalys_sector= value_non_aggreg * dalys / value_aggreg
        
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    #                             else:
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    #                         elif (year==2009):
    #                             value_aggreg = float(distrib_aggreg_homme_exio3.loc[(distrib_aggreg_homme_exio3['EXIO3']==code)&(distrib_aggreg_homme_exio3['time']==year),['Value']].to_string(header=False, index=False))
    #                             value_non_aggreg = float(distrib_sector_homme_exio3.loc[(distrib_sector_homme_exio3['EXIO3']==code)&(distrib_sector_homme_exio3['time']==year)&(distrib_sector_homme_exio3['Sector']==c),['Value']].to_string(header=False, index=False))
    #                             dalys_2009 = daly_2010-delta_daly
    #                             if (dalys_2009 > 0 and value_aggreg !=0):
    #                                 dalys_sector= value_non_aggreg * dalys_2009 / value_aggreg
        
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    #                             else:
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
                            
    #                         elif (year>2010 and year <2016):
    #                             value_aggreg = float(distrib_aggreg_homme_exio3.loc[(distrib_aggreg_homme_exio3['EXIO3']==code)&(distrib_aggreg_homme_exio3['time']==year),['Value']].to_string(header=False, index=False))
    #                             value_non_aggreg = float(distrib_sector_homme_exio3.loc[(distrib_sector_homme_exio3['EXIO3']==code)&(distrib_sector_homme_exio3['time']==year)&(distrib_sector_homme_exio3['Sector']==c),['Value']].to_string(header=False, index=False))
    #                             dalys = daly_2010+(year-2010)*delta_daly
    #                             if (dalys>0 and value_aggreg !=0):
                                    
    #                                 dalys_sector= value_non_aggreg * dalys / value_aggreg
        
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    #                             else:
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    #                         else :
    #                             value_aggreg = float(distrib_aggreg_homme_exio3.loc[(distrib_aggreg_homme_exio3['EXIO3']==code)&(distrib_aggreg_homme_exio3['time']==year),['Value']].to_string(header=False, index=False))
    #                             value_non_aggreg = float(distrib_sector_homme_exio3.loc[(distrib_sector_homme_exio3['EXIO3']==code)&(distrib_sector_homme_exio3['time']==year)&(distrib_sector_homme_exio3['Sector']==c),['Value']].to_string(header=False, index=False))
    #                             dalys_2010_2016 = daly_2016+delta_daly
    #                             if dalys_2010_2016 > 0:
    #                                 if (value_aggreg != 0):
    #                                     dalys_sector= value_non_aggreg * dalys_2010_2016 / value_aggreg
                                    
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[dalys_sector],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])                            
    #                             else:
    #                                 d={'year':[year],'EXIO3': [code],'sector':[c],'Mapping':['ECO_DETAILS_C'],'Sex':[a],'estimate':[b],'DALY (in 1000s)':[0],'exposure':['particulate']}
    #                                 df = pd.DataFrame(data=d)
    #                                 daly_split=pd.concat([daly_split, df])
    
    
    
    
    
    
    
    
    
    
    
    
        
    #daly_split.to_csv('particulate_dalys.csv')
    #daly_split.pivot(index=['ISO3','EXIO3','sector','Mapping','Sex','estimate','exposure'],columns='year',values='DALY (in 1000s)')
    #daly_pivot=daly_split.pivot(index=['ISO3','EXIO3','sector','Mapping','Sex','estimate','exposure'],columns='year',values='DALY (in 1000s)')
    #daly_pivot[2009] = pd.to_numeric(daly_pivot[2009], errors='coerce')
    #daly_pivot[2010] = pd.to_numeric(daly_pivot[2010], errors='coerce')
    #daly_pivot[2011] = pd.to_numeric(daly_pivot[2011], errors='coerce')
    #daly_pivot[2012] = pd.to_numeric(daly_pivot[2012], errors='coerce')
    #daly_pivot[2013] = pd.to_numeric(daly_pivot[2013], errors='coerce')
    #daly_pivot[2014] = pd.to_numeric(daly_pivot[2014], errors='coerce')
    #daly_pivot[2015] = pd.to_numeric(daly_pivot[2015], errors='coerce')
    #daly_pivot[2016] = pd.to_numeric(daly_pivot[2016], errors='coerce')
    #daly_pivot[2017] = pd.to_numeric(daly_pivot[2017], errors='coerce')
    #
    #daly_pivot_interpolate=daly_pivot.interpolate(method='linear',limit_direction='both', axis=1)
    #daly_pivot_interpolate.to_csv('particulate_daly_complete.csv')
    #daly_pivot_back = daly_pivot_interpolate.stack()
    #daly_pivot_back.to_csv('particulate_daly_complete_final.csv')
    #new = pd.read_csv('particulate_daly_complete_final.csv')
    #particulate_daly_complete_final_exio3=new.groupby(['EXIO3','sector','Mapping','Sex','estimate','exposure','year']).sum()
    #particulate_daly_complete_final_exio3.to_csv('particulate_daly_complete_final_exio3.csv')
