def exposure():

    import pandas as pd 
    import country_converter as coco
    import ray
    from pathlib import Path
    import os
    import sys
    sys.path.insert(2, 'all_scripts_in_serie')



    import diesel
    import cadmium
    import sulfuric
    import gases
    import ergonomic
    import formaldehyde
    import nickel
    import benzene
    import chromium
    import asthmagens
    import asbestos
    import hydrocarbons
    import silica
    import beryllium
    import noise
    import arsenic
    import trichloroethylene

    storage_group_root = Path(".").absolute()
    data_cleaned_path = storage_group_root / "clean_data"

    list_exposure=[]
    
    
    
    for a in os.listdir(data_cleaned_path):
        list_exposure.append(a.replace("_cleaned.csv",""))

    if "injuries" in list_exposure:
        list_exposure.remove('injuries')
    if "hours" in list_exposure:
        list_exposure.remove('hours')   
        
        
    """Unwanted results related to some exposure can be avoid by commenting out 
    "exposure".exposure()
    """
    
    diesel.exposure()
    cadmium.exposure()
    sulfuric.exposure()
    gases.exposure()
    ergonomic.exposure()
    formaldehyde.exposure()
    nickel.exposure()
    benzene.exposure()
    chromium.exposure()
    asthmagens.exposure()
    asbestos.exposure()
    hydrocarbons.exposure()
    silica.exposure()
    beryllium.exposure()
    noise.exposure()
    arsenic.exposure()
    trichloroethylene.exposure()

        
        

        
