import pandas as pd 
import country_converter as coco
import ray
import os


def exposure():
    mapping_injuries = pd.read_excel('aux/Mapping_Injuries_ILO-WHO_EXIOBASE_2023.xlsx','Sheet1')
    workforce_iso3 = pd.read_csv('aux/table_workforce_by_ISO3.csv')
    workforce_exio3 = pd.read_csv('aux/table_workforce_by_EXIO3.csv')
    
    workforce_iso3 = workforce_iso3.drop(columns=['ref_area.label','indicator','source','obs_status'])
    formaldehyde = pd.read_csv('clean_data/formaldehyde_cleaned.csv')
    
    concordance = pd.read_excel('aux/Exiobase_ISIC_Rev-4.xlsx','ILO_mapping_sector') 
    
    classif_detail = [s for s in workforce_iso3['classif1'].unique() if ("DETAILS" in s and "TOTAL" not in s)]
    
    columns_occ_injuries = ['ISO3','Sector','Mapping','Sex','Year','Value']
    occupational_injuries_split = pd.DataFrame(columns=columns_occ_injuries)
    mapping_true=[]
    cc_all = coco.CountryConverter(include_obsolete=True)
    distribution_sector_homme = pd.DataFrame(columns=['ISO3','time','Sector','Value'])
    distribution_sector_homme_final = pd.DataFrame(columns=['ISO3','time','Sector','Value'])
    
    distribution_sector_femme = pd.DataFrame(columns=['ISO3','time','Sector','Value'])
    distribution_sector_femme_final = pd.DataFrame(columns=['ISO3','time','Sector','Value'])
    for a in classif_detail : 
        list_name = concordance.loc[concordance['ISIC REV 4_ILO_Alteryx']==a,['Name']]
        for b in list_name['Name']:
            if (mapping_injuries.loc[mapping_injuries['Unnamed: 1']==b, 'Unnamed: 31'].to_string(header=False,index=False)=='1'):
                print(b)
                mapping_true.append(b)
                    
                    
    ray.init(num_cpus = os.cpu_count()-1)
    
    @ray.remote
    
    def calcul_distribution(code, distribution_sector_femme,distribution_sector_homme):
        print("country : ", code)    
        EXIO_code = cc_all.convert(names = code,src="ISO3", to='EXIO3')
        distribution_sector_homme = pd.DataFrame(columns=['ISO3','time','Sector','Value'])
        distribution_sector_femme = pd.DataFrame(columns=['ISO3','time','Sector','Value'])
                    
    
        for year in range(2009,2018):
            #print(year)
            split_year  = pd.read_excel('aux/split.xlsx',str(year))
    
            for a in classif_detail : 
                list_name = concordance.loc[concordance['ISIC REV 4_ILO_Alteryx']==a,['Name']]
                for b in list_name['Name']:
                    if b in mapping_true:
                        #print(b)
    
                        for c in workforce_iso3['sex'].unique():
                            if c=='SEX_F':
                                pop_sex_iso3_mapping = float(workforce_iso3.loc[(workforce_iso3['ref_area']==code)&(workforce_iso3['EXIO3']==EXIO_code)&(workforce_iso3['sex']==c)&(workforce_iso3['classif1']==a)&(workforce_iso3['time']==year),['obs_value']].to_string(header=False,index=False))
                                pop_sex_iso3_mapping = 1000 * pop_sex_iso3_mapping
                                pop_sex_exio3_sector = float(split_year.loc[(split_year['Country']==EXIO_code)&(split_year['Sector']==b)&(split_year['Mapping']==a),['Female']].to_string(header=False,index=False))
                                pop_sex_exio3_mapping = float(workforce_exio3.loc[(workforce_exio3['EXIO3']==EXIO_code)&(workforce_exio3['sex']==c)&(workforce_exio3['classif1']==a)&(workforce_exio3['time']==year),['obs_value']].to_string(header=False,index=False))
                                pop_sex_exio3_mapping = 1000 * pop_sex_exio3_mapping
                                pop_missing = (pop_sex_exio3_sector/pop_sex_exio3_mapping)*pop_sex_iso3_mapping 
    
                                
                                d={'ISO3':[code],'time': [year],'Sector':[b],'Value': [pop_missing]}
                                df = pd.DataFrame(data=d)
                                distribution_sector_femme=pd.concat([distribution_sector_femme, df])
    
    
                            if c=='SEX_M':
                                pop_sex_iso3_mapping = float(workforce_iso3.loc[(workforce_iso3['ref_area']==code)&(workforce_iso3['EXIO3']==EXIO_code)&(workforce_iso3['sex']==c)&(workforce_iso3['classif1']==a)&(workforce_iso3['time']==year),['obs_value']].to_string(header=False,index=False))
                                pop_sex_iso3_mapping = 1000 * pop_sex_iso3_mapping
                                pop_sex_exio3_sector = float(split_year.loc[(split_year['Country']==EXIO_code)&(split_year['Sector']==b)&(split_year['Mapping']==a),['Male']].to_string(header=False,index=False))
                                pop_sex_exio3_mapping = float(workforce_exio3.loc[(workforce_exio3['EXIO3']==EXIO_code)&(workforce_exio3['sex']==c)&(workforce_exio3['classif1']==a)&(workforce_exio3['time']==year),['obs_value']].to_string(header=False,index=False))
                                pop_sex_exio3_mapping = 1000 * pop_sex_exio3_mapping
                                pop_missing = (pop_sex_exio3_sector/pop_sex_exio3_mapping)*pop_sex_iso3_mapping 
    
                                
                                d={'ISO3':[code],'time': [year],'Sector':[b],'Value': [pop_missing]}
                                df = pd.DataFrame(data=d)
                                distribution_sector_homme=pd.concat([distribution_sector_homme, df])                        
    
        return distribution_sector_femme,distribution_sector_homme
    
    results = [ray.get([calcul_distribution.remote(code,distribution_sector_femme,distribution_sector_homme) for code in workforce_iso3['ref_area'].unique()])]
                        
    
    for a in results : 
        for i in range(0,len(results[0])):
            print(i)
    
            distribution_sector_femme_final=distribution_sector_femme_final.append([a[i][0]])
            distribution_sector_homme_final=distribution_sector_homme_final.append([a[i][1]])
        
        
    
    ray.shutdown()
        
    distribution_sector_femme_final_aggregation = distribution_sector_femme_final.groupby(['ISO3','time']).sum()
    distribution_sector_homme_final_aggregation = distribution_sector_homme_final.groupby(['ISO3','time']).sum() 
    
    distribution_sector_femme_final_aggregation.to_csv('distribution_data/formaldehyde_distribution_femme_sector_aggregation.csv')
    distribution_sector_homme_final_aggregation.to_csv('distribution_data/formaldehyde_distribution_homme_sector_aggregation.csv')
    distribution_sector_femme_final.to_csv('distribution_data/formaldehyde_distribution_sector_femme.csv')
    distribution_sector_homme_final.to_csv('distribution_data/formaldehyde_distribution_sector_homme.csv')

