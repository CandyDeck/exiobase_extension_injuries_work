# Occupational Injuries at Work

Ensuring social data's reliability is essential in accurately evaluating social and economic impacts across geographical locations, economic sectors and stakeholder categories. Yet, the MRIO model utilized in our research (EXIOBASE) was hindered by out-of-date or significantly proxy fatality statistics, causing potential inaccuracies in our findings. We have comprehensively revised EXIOBASE fatality data to address this shortcoming, incorporating detailed, nation- specific, and up-to-date data. The update includes work-related fatal occupational injuries as well as fatalities associated with occupational exposure to a variety of 18 hazardous substances and conditions such as asbestos, arsenic, benzene, beryllium, cadmium, chromium, diesel engine exhaust, formaldehyde, nickel, polycyclic aromatic hydrocarbons, silica, sulfuric acid, trichloroethylene, asthmagens, particulate matter, gases and fumes, noise, ergonomic factors, and prolonged working hours. Our methodological process is built on three pillars: data acquisition, raw data processing, and computation of fatal injuries by country, gender, year, and EXIOBASE economic sector. 

Data were sourced from the World Health Organization (WHO)5 (Pega et al., 2021) and Eurostat databases6 (Publications Office of the European Union, 2013). The WHO data was carefully screened based on specific criteria such as age above 15 years, gender, and fatal injuries only. Eurostat data provided granular information on work-related fatalities, classified by economic activities in the European Community (or NACE Rev.2 (Eurostat, 2008)). The WHO provided aggregate fatality data for 2010 and 2016. The strategy for allocating these deaths across Eurostat categories depended on the countries' geographical location, with different methods applied to European and non-European nations. 

For European nations, fluctuations in fatality numbers within a NACE Rev.2 sector mirrored the changes registered by Eurostat. For non-European countries, fatality figures were proportionally allocated across economic sectors split according to the NACE Rev.2 classification, reflecting the workforce size associated with each economic sector. Due to the scarcity of data for nations within Asia, America, or Africa, we adopted a regional approach, computing fatality ratios over each NACE Rev.2 category for each region by integrating data for available countries over a reference year. For 2010 and 2016, the aggregate fatality figures for nations within these three zones were established. Due to the temporal proximity of both reference years, we postulated a linear trend in the fatality count between these two years. The number of fatalities for a specific country, year, and per NACE Rev.2 activity was then calculated by applying the previously mentioned fatality ratio to the total number of deaths for that nation. Last, we applied the European annual ratios to their total mortality figures for the few countries that could not be classified as European or belonging to one of the aforementioned zones. 

The result is a comprehensive database that includes the number of fatalities (expressed in the number of deaths for work-related fatal occupational injuries and in Disability-adjusted life years (DALYs), for fatalities associated with occupational exposure to a specific risk factor), detailed at the country, gender, and NACE Rev.2 sector levels from 2008 to 2019, providing insights into work-related fatal injuries across different health effects and geographical regions. The entire database is readily available in the Supplementary Information (SI). 

# Scripts

First of all, this script uses [ray](https://www.ray.io/).
This has been set up in the whole script so that all but 1 CPUs are used.

**run_all.py** is the main script.



This script is devided into 3 parts :


```python
# Step 1 - downloading and cleanig the data
download_module.download_and_clean.download_clean()
# Step 2 - processing the raw data related to each exposure
all_scripts_in_serie.exposure_all.exposure()
# Step 3 - producing the final table, one per occupational injurie
final_daly_exposure.final()
```
## Download and Clean

The **download_and_clean.py** includes all exposures.
If one does not want to look at all exposures, one has to comment out part of the script.

For exemple if one wants to look at all exposure exept **asbestos**, one has to comment out these 2 groups of lines :

```python
    src_url = get_url.asbestos()
    src_csv = Path("asbestos.csv")
    download_data(src_url,src_csv,data_path)
```

and :

```Python
    asbestos = pd.read_csv(data_path/"asbestos.csv", encoding="utf-8-sig") 

    asbestos = asbestos[asbestos['Age group'].str.contains('≥15 years',regex=True)]
    asbestos = asbestos[asbestos['Sex'].str.contains('Male|Female',regex=True)]
    asbestos = asbestos[asbestos['Measure'].str.contains('Disability-adjusted life years',regex=True)]
    asbestos=asbestos.drop(columns=['Popultation-attributable fraction (%)','Rate (per 100,000 of population)'])
    asbestos = asbestos[asbestos['Year'] >1994]

    country_code = list(asbestos['Country'])

    asbestos.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
    asbestos.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
    asbestos = asbestos[~asbestos['EXIO3'].str.contains('not found',regex=True)]
    asbestos=asbestos.drop(columns=['Risk Factor','Age group'])

    asbestos.to_csv(data_cleaned_path/"asbestos_cleaned.csv",index=False)
```

**download_and_clean.py** download the raw data from [WHO](https://who-ilo-joint-estimates.shinyapps.io/OccupationalBurdenOfDisease/) and remove the unnecessary data and produce a clean version of the data called **"exposure"_clean.csv**.

## Process the clean data 
Base on the [workforce](https://gitlab.com/CandyDeck/exiobase_extension_injuries_work/-/blob/main/aux/table_workforce_by_ISO3.csv?ref_type=heads) (by ISO3 code, sex, NACE sector amd year):

| ref_area | ref_area.label | EXIO3 | indicator           | source  | sex   | classif1         | time | obs_value |
| -------- | -------------- | ----- | ------------------- | ------- | ----- | ---------------- | ---- | --------- |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_A    | 1991 | 1515.581  |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_B    | 1991 | 5.726     |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_C    | 1991 | 86.432    |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_DE   | 1991 | 3.283     |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_F    | 1991 | 203.412   |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_G    | 1991 | 195.335   |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_HJ   | 1991 | 97.566    |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_I    | 1991 | 5.703     |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_K    | 1991 | 1.493     |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_LMN  | 1991 | 7.699     |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_O    | 1991 | 142.429   |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_P    | 1991 | 13.012    |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_Q    | 1991 | 180.128   |
| AFG      | Afghanistan    | WA    | EMP_2EMP_SEX_ECO_NB | XA:2198 | SEX_M | ECO_DETAILS_RSTU | 1991 | 21.888    |

**exposure_all.py** evaluate the population (by country, sex, year, EXIOBASE sectors) exposed (for each exposure).

if one is not interested in one exposure, one just have to comment out the exposure :

```python 
    diesel.exposure()
    cadmium.exposure()
    sulfuric.exposure()
    gases.exposure()
    ergonomic.exposure()
    formaldehyde.exposure()
    nickel.exposure()
    benzene.exposure()
    chromium.exposure()
    asthmagens.exposure()
    asbestos.exposure()
    hydrocarbons.exposure()
    silica.exposure()
    beryllium.exposure()
    noise.exposure()
    arsenic.exposure()
    trichloroethylene.exposure()
```

From this, we get 4 files per exposure.

"exposure"_distribution_sector_homme.csv
"exposure"_distribution_sector_femme.csv
Both of them gives, for defined years, ISO code, sex and exiobase sector, the number of persons subject to an exposure.

for exemple this table summarizes the number of people exposed to trichloroethylene:
| ISO3 | time | Sector                                                                        | Value            |
| ---- | ---- | ----------------------------------------------------------------------------- | ---------------- |
| AFG  | 2009 | Manufacture of fabricated metal products, except machinery and equipment (28) | 14051.4025409222 |
| AFG  | 2009 | Manufacture of machinery and equipment n.e.c. (29)                            | 30300.347695083  |
| AFG  | 2009 | Manufacture of office machinery and computers (30)                            | 2358.38403386868 |
| AFG  | 2009 | Manufacture of electrical machinery and apparatus n.e.c. (31)                 | 14688.6347301715 |


"exposure"_distribution_homme_sector_aggregation.csv
"exposure"_distribution_femme_sector_aggregation.csv

Both of them gives, for defined years, ISO code, sex the number of persons subject to an exposure.

for exemple this table summarizes the total number of people(men in this case), exposed to trichloroethylene per year in Afganisthan:

| ISO3 | time | Value            |
| ---- | ---- | ---------------- |
| AFG  | 2009 | 61398.7690000453 |
| AFG  | 2010 | 65774.9094770138 |
| AFG  | 2011 | 69349.0399538084 |
| AFG  | 2012 | 74876.2978363615 |
| AFG  | 2013 | 68973.3397562554 |
| AFG  | 2014 | 81605.6739147893 |
| AFG  | 2015 | 83957.3728630702 |
| AFG  | 2016 | 81577.9592778837 |
| AFG  | 2017 | 84035.1091694106 |


## Final table

The third part of the script, **final_daly_exposure.py**, creates tables, one per exposure, called **"exposure"_daly_iso3_ray.csv**
This table, gives, for a given year, ISO3 code, exiobase sector, corresponding NACE rev.2 sector, sex, estimate and exposure, the DALY (in 1000s) :

| year | ISO3 | EXIO3 | sector                                                                                                              | Mapping       | Sex    | estimate | DALY (in 1000s)   | exposure |
| ---- | ---- | ----- | ------------------------------------------------------------------------------------------------------------------- | ------------- | ------ | -------- | ----------------- | -------- |
| 2009 | AFG  | WA    | Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries | ECO_DETAILS_C | Female | Lower    | 7.71194996048712  | diesel   |
| 2009 | AFG  | WA    | Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries | ECO_DETAILS_C | Male   | Lower    | 277.638396928255  | diesel   |
| 2009 | AFG  | WA    | Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries | ECO_DETAILS_C | Female | Point    | 11.1394832762592  | diesel   |
| 2009 | AFG  | WA    | Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries | ECO_DETAILS_C | Male   | Point    | 288.911282864647  | diesel   |
| 2009 | AFG  | WA    | Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries | ECO_DETAILS_C | Female | Upper    | 15.1382721446599  | diesel   |
| 2009 | AFG  | WA    | Sale, maintenance, repair of motor vehicles, motor vehicles parts, motorcycles, motor cycles parts and accessoiries | ECO_DETAILS_C | Male   | Upper    | 300.184168801039  | diesel   |
| 2009 | AFG  | WA    | Retail sale of automotive fuel                                                                                      | ECO_DETAILS_C | Female | Lower    | 0.101394369188889 | diesel   |
| 2009 | AFG  | WA    | Retail sale of automotive fuel                                                                                      | ECO_DETAILS_C | Male   | Lower    | 3.65030408010957  | diesel   |
| 2009 | AFG  | WA    | Retail sale of automotive fuel                                                                                      | ECO_DETAILS_C | Female | Point    | 0.146458533272839 | diesel   |
| 2009 | AFG  | WA    | Retail sale of automotive fuel                                                                                      | ECO_DETAILS_C | Male   | Point    | 3.79851651031912  | diesel   |
| 2009 | AFG  | WA    | Retail sale of automotive fuel                                                                                      | ECO_DETAILS_C | Female | Upper    | 0.199033391370782 | diesel   |
| 2009 | AFG  | WA    | Retail sale of automotive fuel                                                                                      | ECO_DETAILS_C | Male   | Upper    | 3.94672894052868  | diesel   |
| 2009 | AFG  | WA    | Supporting and auxiliary transport activities; activities of travel agencies (63)                                   | ECO_DETAILS_C | Female | Lower    | 5.68665567032399  | diesel   |
| 2009 | AFG  | WA    | Supporting and auxiliary transport activities; activities of travel agencies (63)                                   | ECO_DETAILS_C | Male   | Lower    | 116.877965660545  | diesel   |
| 2009 | AFG  | WA    | Supporting and auxiliary transport activities; activities of travel agencies (63)                                   | ECO_DETAILS_C | Female | Point    | 8.21405819046799  | diesel   |
| 2009 | AFG  | WA    | Supporting and auxiliary transport activities; activities of travel agencies (63)                                   | ECO_DETAILS_C | Male   | Point    | 121.623533960701  | diesel   |
| 2009 | AFG  | WA    | Supporting and auxiliary transport activities; activities of travel agencies (63)                                   | ECO_DETAILS_C | Female | Upper    | 11.1626944639693  | diesel   |
| 2009 | AFG  | WA    | Supporting and auxiliary transport activities; activities of travel agencies (63)                                   | ECO_DETAILS_C | Male   | Upper    | 126.369102260857  | diesel   |
